package adapters;

import java.util.ArrayList;

import token.Token;
import user.User;

public interface AdapterDB 
{
	public void update(User key, ArrayList<Token> value);
	public ArrayList<Token> getTokens(User key);
	public boolean remove(User user);
	public boolean query(User user);
	public ArrayList<User> getAllUsers();
	public User getUser(String userName);
	public Token getToken(String tokenValue);
}
