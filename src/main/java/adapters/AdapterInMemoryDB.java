package adapters;
import java.util.ArrayList;
import java.util.Hashtable;

import token.Token;
import user.User;

public class AdapterInMemoryDB implements AdapterDB 
{
	private Hashtable<User, ArrayList<Token>> hashtable = new Hashtable<User, ArrayList<Token>>();
	
	@Override
	public boolean remove(User key)
	{
		return (key != null) ? (hashtable.remove(key) != null)  : false;
	}
	
	@Override
	public boolean query(User key)
	{
		return (key != null) ? hashtable.containsKey(key)  : null;
	}

	
	@Override
	public void update(User key, ArrayList<Token> value)
	{
		if (key != null && value != null)
			hashtable.put(key, value);
	}

	@Override
	public ArrayList<Token> getTokens(User key)
	{
		return (key != null) ? hashtable.get(key) : null;
	}

	@Override
	public ArrayList<User> getAllUsers() {
		return new ArrayList<User>(hashtable.keySet());
	}

	@Override
	public User getUser(String userName) 
	{
		ArrayList<User> userList = getAllUsers();
		for( User u : userList )
		{
			if( u.getUserName() == userName )
				return u;
		}
		return null;
	}
	
	@Override
	public Token getToken(String tokenValue) 
	{	
		ArrayList<User> userList = new ArrayList<User>(hashtable.keySet());
		for( User u : userList )
		{
			ArrayList<Token> tokenList = getTokens(u);
			for( Token t : tokenList )
			{
				if( t.getIDhashcode() == Integer.parseInt(tokenValue) )
					return t;
			}
		}
		return null;
	}
}
