package adapters;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;


import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import logic.TokenHandler;

import org.json.*;

@Path("/tokens")
public class AdapterREST
{
	public static TokenHandler tokenHandler = new TokenHandler();
	
	// GET
	@Path("/test")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response test()  
	{
		return  Response
				.status(Response.Status.OK)
				.type("text/plain")
                .entity("test")
                .build();
	}
	
	// PUT
	@Path("/consume")
	@PUT
	@Consumes("application/json")
	public Response consume(Object data) 
	{
	    return Response
	    	      .status(Response.Status.OK)
	    	      .entity("Put JSONObject here!")
	    	      .build();
	}

	// POST 1-5 tokens
	@Path("/generator")
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response requestTokens(@PathParam("tokenid") String user, @PathParam("tokenid") String tokenAmount) {
		try {
			JSONObject jObject = new JSONObject(tokenHandler.getTokens( user, Integer.parseInt(tokenAmount) ));
			return  Response
					.status(Response.Status.OK)
					.entity(jObject)
					.build();
		} catch (Exception e) {
		throw new BadRequestException(
					Response
						.status(Response.Status.BAD_REQUEST)
						.entity(e.getMessage())
						.build());
		}
	}

}
