package user;

public class BasicUser implements User
{
	private long id;
	private String name;
	private static long count = 0;
	
	public BasicUser(String name) {
		this.id = count;
		this.name = name;
		count++;
	}
	@Override
	public Object getUserID() {
		return id;
	}
	
	public String getUserName() {
		return name;
	}
	
}
