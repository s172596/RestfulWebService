package logic;

import java.util.ArrayList;
import java.util.UUID;

import adapters.AdapterDB;
import adapters.AdapterInMemoryDB;
import token.Token;
import token.TokenImplementation;
import user.User;

public class TokenHandler
{
	private AdapterDB adapterDB = new AdapterInMemoryDB();

	
	/**
	 * If the user has 1 or less tokens then 1-5 tokens can be generated.
	 * 
	 * @param userName - name of user
	 * @param amm - the amount desired between 1-5
	 * @author s164166 Patrick
	 * @return amm new tokens or an empty list.
	 */
	public ArrayList<Token> getTokens(String userName, int amm)
	{
		User user = adapterDB.getUser(userName);	
		return ((checkValidAmmount(amm)) ? generateTokens(user, amm) : new ArrayList<Token>() );
	}
	
	
	/**
	 * Verifies whether a token is legit.
	 * @param token being verified
	 * @return token existence
	 * @author s164166 Patrick
	 */
	public boolean tokenExists(Token token)
	{
		return adapterDB.getAllUsers().parallelStream().anyMatch(currUser -> adapterDB.getTokens(currUser).contains(token));
	}

	/**
	 * Consumes a token marking it as having been used.
	 * If fake token is given it returns false.
	 * @param tokenValue
	 * @return whether token was consumed or not
	 * @author s164166 Patrick
	 */
	public boolean consume(String tokenValue)
	{
		Token token = adapterDB.getToken(tokenValue);
		return tokenExists(token) ? consumeTokenHandling(token) : false;
	}
	
	/**
	 * If the user exists in the database it returns a list of their tokens, otherwise an empty list is returned
	 * @param user
	 * @return
	 * @author s164166 Patrick
	 */
	public ArrayList<Token> getUserTokens(User user)
	{
		return (checkUserExists(user)) ? adapterDB.getTokens(user) : new ArrayList<Token>();
	}
	
	/**
	 * Returns the amount of tokens a user has in total
	 * @param user
	 * @return
	 * @author s164166 Patrick
	 */
	public long checkTokenCount(User user)
	{
		return adapterDB.getTokens(user).size();
	}
	
	/**
	 * Filters the list returning only unused tokens
	 * @param tokens - unfiltered list
	 * @return tokens - filtered list
	 * @author s164166 Patrick
	 */
	public ArrayList<Token> getUnusedTokens(ArrayList<Token> tokens)
	{
		ArrayList<Token> collection = new ArrayList<Token>();
		if (tokens.size() > 0)
			for (Token token : tokens)
			{
				if  (!token.getTokenStatus())
					collection.add(token);
			}
		return collection;
	}
	
	/**
	 * Invoked in consume to mark whether it was used or not
	 * @param token - token being consumed
	 * @return
	 * @author s164166 Patrick
	 */
	private boolean consumeTokenHandling(Token token)
	{
		if (!token.getTokenStatus())
		{
			token.useToken();
			return true;
		}
		return false;
	}
	
	/**
	 * Used in GetTokens to generate the tokens if the user follows the requirements
	 * @param user - user token is being generated for
	 * @param amm - the amount of tokens being generated
	 * @return A list of tokens, if the list is empty it was an illegal ammount or the user had too many
	 * @author s164166 Patrick
	 */
	private ArrayList<Token> generateTokens(User user, int amm )
	{
		ArrayList<Token> tokens = new ArrayList<Token>();
		if(checkValidGainToken( getUserTokens(user)))
			tokens = generateNewTokens(tokens, user, amm);
		return tokens;
	}
	
	/**
	 * Generates the tokens and updates the database
	 * @param tokens list being updated
	 * @param user user being updated
	 * @param amm the amount of tokens
	 * @return list of newly generated Tokens
	 * @author s164166 Patrick
	 */
	private ArrayList<Token> generateNewTokens(ArrayList<Token> tokens, User user, int amm )
	{
		for (int i = 0; i < amm; i++)
		{	
			tokens.add(getNewToken(user.getUserName()));
		}
		adapterDB.update(user, tokens);
		return tokens;
	}	
	
	/**
	 * Checks if a user exists
	 * @author s164166 Patrick
	 * @param user
	 * @return if user exists return true otherwise false
	 */
	private boolean checkUserExists(User user)
	{
		return adapterDB.query(user);
	}

	/**
	 * Creates a new token that is returned
	 * It uses UUID to generate random unique user IDs
	 * @param userName
	 * @return TokenImplementation
	 * @author s164166 Patrick
	 */
	private Token getNewToken(String userName)
	{
		String hash = UUID.randomUUID().toString();
		return new TokenImplementation(hash, userName);
	}
	
	/**
	 * Verifies that the amount of tokens is valid
	 * @param amm
	 * @return true if amm is between 1-5 otherwise false
	 * @author s164166 Patrick
	 */
	private boolean checkValidAmmount( int amm )
	{
		return (amm > 0 && amm < 6);		
	}

	/**
	 * Verifies that the amount of tokens that are unused is 1 or less
	 * @param tokens list of tokens being checked
	 * @return true if unused token ammount is 0 or 1.
	 * @author s164166 Patrick
	 */
	private boolean checkValidGainToken(ArrayList<Token> tokens)
	{
		return (getUnusedTokens(tokens).size() <= 1);
	}
}