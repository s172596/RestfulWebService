package database;

import java.util.Hashtable;

public class InMemoryDB implements Database
{
	private Hashtable<Object, Object> hashtable; 
	
	public InMemoryDB() {
		hashtable = new Hashtable<Object, Object>();		
	}

	@Override
	public Hashtable<Object, Object> getConnection() 
	{
		return hashtable;
	}
}
