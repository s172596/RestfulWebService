package database;

import java.util.Hashtable;

public interface Database {
	
	public Hashtable<Object, Object> getConnection();

}
