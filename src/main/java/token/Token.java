package token;

public interface Token
{
	public Object getLocation();
	public int getIDhashcode();
	public boolean getTokenStatus();
	public void useToken();
}
