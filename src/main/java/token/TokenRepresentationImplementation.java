package token;

import java.net.URI;

public class TokenRepresentationImplementation implements TokenRepresentation
{
	private TokenID id;
	private URI path;
	
	/**
	 * Generates a representation of the token.
	 * @param idString, unique string for ID.
	 * @param path path to barcode.
	 * @author s164166 Patrick
	 */
	public TokenRepresentationImplementation(String idString, URI path)
	{
		id = new TokenIDImplementation(idString);
		this.path = path;
	}
	
	/**
	 * returns the path of the URI which contains the barcode associated with the represented token
	 * @return URI to barcode
	 * @author s164166 Patrick
	 */
	@Override
	public URI getPath()
	{
		return path;
	}
	
	/**
	 * Returns the hashcode for the tokenID to check for co
	 * @return
	 * @author s164166 Patrick
	 */
	public int getIDhashcode()
	{
		return id.hashCode();
	}
	
	/**
	 * Compare two IDs to check if they are equal
	 * @param obj Object 
	 * @author s164166 Patrick
	 */
	@Override
	public boolean equals(Object obj)
	{
		return id.equals(obj);
		
	}
	
	/**
	 * Gets the tokenID of the object
	 * @return
	 * @author s164166 Patrick
	 */
	public TokenID getTokenID()
	{
		return id;
	}

}
