package token;

import java.net.URI;

public interface TokenRepresentation
{
	public URI getPath();
	public int getIDhashcode();
}