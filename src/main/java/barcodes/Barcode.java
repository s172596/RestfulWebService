package barcodes;

import java.io.IOException;

public interface Barcode
{
	public Object generateBarcode(String token, String userName) throws IOException;
}
