package barcodes;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.krysalis.barcode4j.HumanReadablePlacement;
import org.krysalis.barcode4j.impl.code39.Code39Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;

public class Barcode4JGenerator implements Barcode
{
	private Code39Bean bean = new Code39Bean();
	private final int dpi = 150;
	
	
	@Override
	public Object generateBarcode(String token, String userName) throws IOException
	{
		String location;
		bean.setModuleWidth(UnitConv.in2mm(1f /dpi));
		bean.setWideFactor(3);
		bean.doQuietZone(false);
		bean.setMsgPosition(HumanReadablePlacement.HRP_NONE);
		
		//TODO come back to change naming convention from "token.png" to "token_userName.png" )
		location = "resources/barcodes/"+ token +"_"+userName+".png";
		
		File outputFile = new File(location);
        OutputStream out = new FileOutputStream(outputFile);
        try 
        {
            //Set up the canvas provider for monochrome PNG output
            BitmapCanvasProvider canvas = new BitmapCanvasProvider(out, "image/x-png", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);
            //Generate the barcode
            bean.generateBarcode(canvas, token);
            //Signal end of generation
            canvas.finish();
        } finally {
            out.close();
        }
        return location;
	}
}
