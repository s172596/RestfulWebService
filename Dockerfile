FROM fabric8/java-alpine-openjdk8-jre
COPY target/restfulWebService-thorntail.jar /usr/src/
WORKDIR /usr/src/
RUN echo $(ls -al)
CMD java -Djava.net.preferIPv4Stack=true \
-Djava.net.preferIPv4Addresses=true \
-jar restfulWebService-thorntail.jar